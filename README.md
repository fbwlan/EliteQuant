# EliteQuant
量化投资交易资源汇总

除了列在这里和[国外资源列表](https://github.com/EliteQuant/EliteQuant)之外，还有很多有价值的在线资源。我们不可能全举出所有资源。如果您认为有什么是值得推荐的，请随时与我们联系。一般来说开源项目加入前至少在码云上收到10颗星。

* [量化交易平台](#量化交易平台)
* [交易系统](#交易系统)
* [量化模型](#量化模型)
* [交易API](#交易api)
* [数据源](#数据源)
* [网站论坛博客](#网站论坛博客)
* [论文书籍](#论文书籍)

- - -

## 量化交易平台
* [Ricequant 量化交易平台](https://www.ricequant.com/) - 支持Python的在线量化交易平台; 类似Quantopian Alphalens的[rqalpha系统](https://github.com/ricequant/rqalpha)

* [优矿 - 通联量化实验室](https://uqer.io/) - [通联数据](https://www.datayes.com/)旗下一个基于Python的在线量化交易平台

* [JoinQuant聚宽量化交易平台](https://www.joinquant.com/) - 一个基于Python的在线量化交易平台

* [京东量化](https://quant.jd.com/) - 京东金融旗下支持Python和Java的在线量化交易平台

* [BigQuant](https://bigquant.com/) - Python人工智能和机器学习的在线量化交易平台

* [镭矿raquant](http://www.raquant.com/) - 一个基于Python的在线量化交易平台

* [果仁网](https://quant.jd.com/) - 回测量化平台

* [Factors](http://factors.chinascope.com/) - 数库多因子量化平台

* [诸葛量化](https://www.gpxtrade.com/) - 量化交易平台

* [BotVS](https://www.botvs.com/) -- 首家支持数字货币的量化平台；[Github主页](https://github.com/botvs)

* [宽狗量化](https://www.gpxtrade.com/) - 量化回测平台

* [点宽网](https://www.digquant.com.cn/) -- 基于MATLAB的量化交易平台; 策略研究软件[Auto-Trader](http://www.atrader.com.cn/)

## 交易系统

* [vnpy](http://www.vnpy.org/) - 一个受欢迎的Python开源交易平台；[社区论坛](http://www.vnpie.com)和[Github代码](https://github.com/vnpy/vnpy)

* [万得大奖章](http://www.dajiangzhang.com/) - [Wind万得](http://www.wind.com.cn/)旗下交易平台

* [文化财经--云量化](https://mq.wenhua.com.cn/) - 文远宽语言采用类C语言的语法

* [金之塔](https://www.weistock.com/) - 线下交易软件

* [Tradeblazer(TB) - 交易开拓者](http://www.tradeblazer.net/) - 期货程序化交易软件平台

* [掘金量化MyQuant](http://www.myquant.cn/) - 多语言量化交易系统；[Github主页](https://github.com/myquant/)

* [MultiCharts 中国版 - 程序化交易软件](https://www.multicharts.cn/)

* [量化交易先锋(TradePioneers(Matlab)](http://bbs.pinggu.org/thread-4501068-1-1.html) - 连接以失效

* [EasyQuant 系列](https://github.com/shidenggui) - Python 系统， 包括[easyquant](https://github.com/shidenggui/easyquant), [easytrader](https://github.com/shidenggui/easytrader), [easyquotation](https://github.com/shidenggui/easyquotation), 和 [easyhistory](https://github.com/shidenggui/easyhistory)

* [实盘易](http://www.iguuu.com/e)（[SDK](https://github.com/sinall/ShiPanE-Python-SDK)）  - 管理通达信等交易终端，提供基于 HTTP 协议的 RESTFul API；各大在线量化交易平台实盘解决方案

* [QUANTAXIS](https://github.com/yutiansut/QUANTAXIS) - Python量化金融框架

* [QuantDigger](https://github.com/QuantFans/quantdigger)  - Python量化交易平台

* [FutureTrade](https://github.com/athmoon/FutureTrade) -- ctp接口

* [CTP-TradeServer](https://github.com/dxtkdxtk/CTP-TradeServer) -- ctp接口

* [CTP-Test](https://github.com/tashaxing/CTPtest) -- [踏沙行的博客](http://blog.csdn.net/u012234115/article/details/70195889)

* [Hikyuu](https://gitee.com/fasiondog/hikyuu) - 基于C++/Python的开源量化交易研究框架

## 量化模型

## 交易API

* [上海期货信息技术有限公司CTP API](http://www.sfit.com.cn/5_2_DocumentDown.htm) - 期货交易所提供的API

* [飞马快速交易平台 - 上海金融期货信息技术有限公司](http://www.cffexit.com.cn/) - 飞马

* [大连飞创信息技术有限公司](http://www.dfitc.com.cn/) - 飞创

* [QuantBox/XAPI2](https://github.com/QuantBox/XAPI2) - 统一行情交易接口第2版


## 数据源
* [TuShare网站](http://tushare.org/) 和 [源代码](https://github.com/waditu/tushare)  - 中文财经数据接口包

* [Historical Data Sources](http://quantpedia.com/Links/HistoricalData) - 一个数据源索引

* [Wind万得资讯-经济数据库](http://www.wind.com.cn/) - 收费

* [国泰安数据服务中心](http://dx.gtarsc.com/) - 收费，以及旗下[量宽网](http://www.gtaquant.com/) 

* [通联数据](https://www.datayes.com/) - 数据和资管平台

* [锐思数据](http://www.resset.cn/) - 收费

* [恒生电子](http://www.hundsun.com/); 和 [恒生聚源](http://www.gildata.com/); [恒生API](https://www.hscloud.cn/) - 收费

* [东方财富](http://www.eastmoney.com/) - 财经网站

* [同花顺](http://www.10jqka.com.cn/) - 财经网站

* [大智慧](http://www.gw.com.cn/) - 财经网站

* [巨灵财经](http://www.genius.com.cn/) - 金融大数据

* [通达信](http://www.tdx.com.cn/) - 交易软件和数据

* [数库金融数据和深度分析API服务](http://developer.chinascope.com/) - 收费

* [R金融数据抓取](https://gist.github.com/yanping) -- 期货，基金，网易财经等

* [量化投资数据篇](https://xueqiu.com/8506830704/72813000) - 博文, 及[2](https://xueqiu.com/8506830704/72979651) 和 [3](https://xueqiu.com/8506830704/73125523)

## 网站论坛博客

* [量邦科技](http://www.quanttech.cn/) - 下含[微量网](http://www.wquant.com/) 和 [大宽网](http://www.daquant.com/)  和 [量邦社区](http://bbs.quanttech.cn/index.html) 

* [量投网(liangtou)](http://kt.liangtou.com/article) - 量化投资论坛

* [点宽网](https://www.digquant.com.cn/) - 提供交易终端[Auto-Trader](http://www.atrader.com.cn/)

* [经管之家](http://bbs.pinggu.org/) - 人大经济论坛

* [宽客俱乐部](http://www.quant-club.com/forum.php) - 量化投资论坛

* [海洋部落](http://www.oceantribe.org/xf/index.php) - 量化投资论坛

* [程序化交易者论坛](http://www.programtrader.net/) - 量化投资论坛

* [量化易](http://www.19lh.com/) - 量化投资论坛

* [况客](https://qutke.com/) - 投资研究社区

* [极宽网](http://www.topquant.vip/) - Python量化

* [盈时数字](http://ysquant.com/) - 量化策略开发

* [MatlabSky](http://www.matlabsky.com/)  - Matlab使用者论坛

* [金融数据处理与分析](https://www.zybuluo.com/fanxy/note/348439) - 复旦大学经济学院 樊潇彦 R语言

* [R的极客理想](http://fens.me/) - R 博客

## 论文书籍